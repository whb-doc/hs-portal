import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI} from './web-api';
import Globals from './globals';
import {Router} from 'aurelia-router';

export class ModuleList {
	static inject() { return [WebAPI, EventAggregator, Router] };

	constructor(api, EventAggregator, Router) {
		this.api = api;
		this.ea = EventAggregator;
		this.router = Router;
		this.modules = [];
		
		this.ea.subscribe("ModuleViewed", module => this.select(module));
		this.ea.subscribe("ModuleUpdated", module => {
			let found = this.modules.find(x => x.id === module.id);
			Object.assign(found, module);
			this.sortModules();
		});
	}

	bind() {
		this.api.getModuleList().then(response => {
			this.modules = response;
			this.sortModules();
			this.router.navigateToRoute('module', {id: this.modules[0].id});
		});
		this.account = Globals.CurrentAccount;
	}

	select(module) {
		this.selectedId = module.id;
		return true;
	}
	
	sortModules() {
		this.modules.sort((x, y) => {
			return y.name < x.name;
		});
	}
	
	deleteModule(module) {
		if(confirm(`Möchten Sie wirklich das Modul ${module.name} löschen?`)) {
			this.api.removeModule(module.id).then(response => {
				if(response) {
					let index = this.modules.indexOf(module);
					this.modules.splice(index, 1);
				}
			});
		}
	}
	
	addModule() {
		this.api.addModule().then(newModule => {
			this.modules.push(newModule);
		});
	}
}