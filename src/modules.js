import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI} from './web-api';
import Globals from './globals';

export class Modules {
	static inject() { return [WebAPI, EventAggregator]; }
	constructor(api, EventAggregator) {
		this.api = api;
		this.ea = EventAggregator;
	}
	
	configureRouter(config, router){
		config.title = 'Modules';
		config.map([
			{ route: '', moduleId: 'no-selection', title: 'Select'},
			{ route: 'module/:id',  moduleId: 'module-detail', name:'module' }
		]);

		this.router = router;
	}
	
	activate() {
		if(!Globals.CurrentAccount) {
			this.ea.publish("Login");
		}
	}
	
	bind() {
		this.api.getNews().then(response => {
			this.newsList = response;
		});
	}
}