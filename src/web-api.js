let latency = 200;
let id = 0;

export function getId(){
  return ++id;
}

let users = [
  {
    id: getId(),
    firstName: 'John',
    lastName: 'Tolkien',
    email: 'tolkien@inklings.com',
    phoneNumber: '08675309',
	username: 'admin',
	password: 'admin',
	role: "1", //admin rights, must be string for binding
  },
  {
    id: getId(),
    firstName: 'Lewis',
    lastName: 'Clive',
    email: 'lewis@inklings.com',
    phoneNumber: '08675310',
	username: 'user',
	password: 'user',
	role: "3", //user rights, must be string for binding
  },
];

let news = [
	{id: getId(), heading: "Neueste Neuigkeit1", date: new Date("04.28.2017"), description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."},
	{id: getId(), heading: "Neuigkeit2", date: new Date("04.14.2017"), description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."},
	{id: getId(), heading: "Neueste Neuigkeit super neu3", date: new Date("03.30.2017"), description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."},
	{id: getId(), heading: "Neueste Neuigkeit was auch immer 4", date: new Date("03.20.2017"), description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."},
];

let modules = [
	{id: getId(), name: "Grundlagen Informatik", shortDesc: "Mathe macht Dick 1 Kurzbeschreibung", firstExecution: "01.04.2009", futureDates: '30.08.2017, 23.09.2017, 03.11.2017', requirements: "keine", means: "Taschenrechner, Formelsammlung", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."}, 
	{id: getId(), name: "Datenstrukturen", shortDesc: "Mathe macht Dick 2 Kurzbeschreibung", firstExecution: "01.04.2009", futureDates: '30.08.2017, 23.09.2017, 03.11.2017', requirements: "keine", means: "Taschenrechner, Formelsammlung", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."}, 
	{id: getId(), name: "Datenbanken", shortDesc: "Mathe macht Dick 3 Kurzbeschreibung", firstExecution: "01.04.2009", futureDates: '30.08.2017, 23.09.2017, 03.11.2017', requirements: "keine", means: "Taschenrechner, Formelsammlung", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."}, 
	{id: getId(), name: "Mathematik", shortDesc: "Mathe macht Dick 4 Kurzbeschreibung", firstExecution: "01.04.2009", futureDates: '30.08.2017, 23.09.2017, 03.11.2017', requirements: "keine", means: "Taschenrechner, Formelsammlung", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."}, 
	{id: getId(), name: "Volkswirtschaftslehre", shortDesc: "Mathe macht Dick 5 Kurzbeschreibung", firstExecution: "01.04.2009", futureDates: '30.08.2017, 23.09.2017, 03.11.2017', requirements: "keine", means: "Taschenrechner, Formelsammlung", description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."}, 
];

export class WebAPI {
  isRequesting = false;
  
	login(username, password){
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let account = users.find(x => x.username == username && x.password == password);
				resolve(account);
				this.isRequesting = false;
			}, latency);
		});
	}
  
	getNews() {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				//JSON.parse(JSON.stringify()) makes a deep copy of hte news object. Otherwise the user would work directly on the original object.
				resolve(JSON.parse(JSON.stringify(news)));
				this.isRequesting = false;
			}, latency);
		});
	}
  
	removeNews(newsId) {
		newsId = parseInt(newsId);
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let newsToRemove = news.find(x => x.id == newsId);
				if(newsToRemove) {
					let index = news.indexOf(newsToRemove);
					news.splice(index, 1);
				}
				resolve(true);
				this.isRequesting = false;
			}, latency);
		});
	}
	
	saveNews(newsToSave) {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let originNews = news.find(x => x.id == newsToSave.id);
				if(originNews) {
					let index = news.indexOf(originNews);
					news[index] = newsToSave;
				}
				else {
					news.unshift(newsToSave);
				}
				resolve(true);
				this.isRequesting = false;
			}, latency);
		});
	}
	
	getModuleList() {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				//JSON.parse(JSON.stringify()) makes a deep copy of hte news object. Otherwise the user would work directly on the original object.
				resolve(JSON.parse(JSON.stringify(modules)));
				this.isRequesting = false;
			}, latency);
		});
	}
	
	getModule(moduleId) {
		moduleId = parseInt(moduleId);
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let module = modules.find(x => x.id == moduleId);
				resolve(JSON.parse(JSON.stringify(module)));
				this.isRequesting = false;
			}, latency);
		});
	}
  
	removeModule(moduleId) {
		moduleId = parseInt(moduleId);
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let moduleToRemove = modules.find(x => x.id == moduleId);
				if(moduleToRemove) {
					let index = modules.indexOf(moduleToRemove);
					modules.splice(index, 1);
				}
				resolve(true);
				this.isRequesting = false;
			}, latency);
		});
	}
	
	saveModule(moduleToSave) {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let originModule = modules.find(x => x.id == moduleToSave.id);
				if(originModule) {
					let index = modules.indexOf(originModule);
					modules[index] = moduleToSave;
				}
				else {
					modules.unshift(moduleToSave);
				}
				resolve(true);
				this.isRequesting = false;
			}, latency);
		});
	}
	
	addModule() {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let newModule = {id: getId(), name: "Neues Modul", shortDesc: "Kurzbeschreibung", firstExecution: "01.01.1990", futureDates: '31.12.2099', requirements: "keine", means: "keine", description: "Ausführliche Beschreibung"};
				modules.push(newModule);
				resolve(newModule);
			}, latency);
		});
	}
	
	getUserList() {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				//JSON.parse(JSON.stringify()) makes a deep copy of hte news object. Otherwise the user would work directly on the original object.
				resolve(JSON.parse(JSON.stringify(users)));
				this.isRequesting = false;
			}, latency);
		});
	}
	
	getUser(userId) {
		userId = parseInt(userId);
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let user = users.find(x => x.id == userId);
				resolve(user ? JSON.parse(JSON.stringify(user)) : undefined);
				this.isRequesting = false;
			}, latency);
		});
	}
  
	removeUser(userId) {
		userId = parseInt(userId);
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let userToRemove = users.find(x => x.id == userId);
				if(userToRemove) {
					let index = users.indexOf(userToRemove);
					users.splice(index, 1);
				}
				resolve(true);
				this.isRequesting = false;
			}, latency);
		});
	}
	
	saveUser(userToSave) {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				let originUser = users.find(x => x.id == userToSave.id);
				if(originUser) {
					if(users.find(x => x.username == userToSave.username && x.id != userToSave.id)) {
						resolve(false);
					}
					else {
						let index = users.indexOf(originUser);
						users[index] = userToSave;
						resolve(true);
					}
				}
				else {
					users.unshift(userToSave);
					resolve(true);
				}
				this.isRequesting = false;
			}, latency);
		});
	}
	
	addUser() {
		this.isRequesting = true;
		return new Promise(resolve => {
			setTimeout(() => {
				const id = getId();
				let newUser = {
					id: id,
					firstName: 'Neuer',
					lastName: 'Benutzer',
					email: 'email@email.com',
					phoneNumber: '0123456789',
					username: 'user' + id,
					password: 'user',
					role: "3", //user rights, must be string for binding
				}
				users.push(newUser);
				resolve(newUser);
			}, latency);
		});
	}
}
