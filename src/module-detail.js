import {EventAggregator} from 'aurelia-event-aggregator'
import {WebAPI} from './web-api';
import {areEqual} from './utility';
import Globals from './globals';

export class ModuleDetail {
	static inject() { return [WebAPI, EventAggregator]; }

	constructor(api, EventAggregator) {
		this.api = api;
		this.ea = EventAggregator;
	}

	activate(params, routeConfig) {
		this.routeConfig = routeConfig;
		this.account = Globals.CurrentAccount;

		return this.api.getModule(params.id).then(module => {
			this.module = module;
			this.routeConfig.navModel.setTitle(module.name);
			this.originalModule = JSON.parse(JSON.stringify(module));
			this.ea.publish("ModuleViewed", this.module);
		});
	}

	get canSave() {
		return !this.api.isRequesting;
	}

	saveModule() {
		this.api.saveModule(this.module).then(response => {
			if(response) {
				this.routeConfig.navModel.setTitle(this.module.name);
				this.originalModule = JSON.parse(JSON.stringify(this.module));
				this.ea.publish("ModuleUpdated", this.module);
			}
		});
	}

	canDeactivate() {
		if (!areEqual(this.originalModule, this.module)){
			let result = confirm('You have unsaved changes. Are you sure you wish to leave?');
			if(!result) {
				this.ea.publish("ModuleViewed", this.module);
			}
			return result;
		}

		return true;
	}
}