import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI} from './web-api';
import Globals from './globals';

export class Login {
	static inject() { return [WebAPI, EventAggregator]; }
	constructor(api, EventAggregator) {
		this.api = api;
		this.ea = EventAggregator;
		this.username = "";
		this.password = "";
		this.hint = "";
		this.success = false;
	}
	
	activate() {
		if(Globals.CurrentAccount) {
			Globals.CurrentAccount = undefined;
			this.ea.publish('LoggedInOut');
			this.hint = "Sie haben sich erfolgreich ausgeloggt.";
		}
	}
	
	login() {
		this.hint = "";
		if (this.username && this.password) {
			this.api.login(this.username, this.password).then(result => {
				if(result) {
					this.success = true;
					Globals.CurrentAccount = result;
					this.ea.publish("LoggedInOut", result);
				}
				else {
					this.hint = `FEHLER: Falsches Passwort für den Benutzernamen ${this.username}`;
				}
			});
		}
		else {
			this.hint = "FEHLER: Bitte geben Sie Benutzername und Passwort an.";
		}
	}
	
	passwordLost() {
		this.api.getUserList().then(result => {
			if(result.find(x => x.username == this.username)) {
				this.hint = `Ihnen wurde soeben ein neues Passwort an Ihre hinterlegte Emailadresse versandt.`;
			}
			else {
				this.hint = `FEHLER: Benutzername "${this.username}" unbekannt.`;
			}
		});
	}
}