import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI} from './web-api';
import Globals from './globals';

export class Users {
	static inject() { return [WebAPI, EventAggregator]; }
	constructor(api, EventAggregator) {
		this.api = api;
		this.ea = EventAggregator;
	}
	
	configureRouter(config, router){
		config.title = 'Users';
		config.map([
			{ route: '', moduleId: 'no-user-selection', title: 'Select'},
			{ route: 'user/:id',  moduleId: 'user-detail', name:'user' }
		]);

		this.router = router;
	}
	
	activate() {
		if(!Globals.CurrentAccount) {
			this.ea.publish("Login");
		}
	}
}