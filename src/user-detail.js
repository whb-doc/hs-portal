import {EventAggregator} from 'aurelia-event-aggregator'
import {WebAPI} from './web-api';
import {areEqual} from './utility';
import Globals from './globals';

export class UserDetail {
	static inject() { return [WebAPI, EventAggregator]; }

	constructor(api, EventAggregator) {
		this.api = api;
		this.ea = EventAggregator;
		this.roles = [{id:"1", name:'Admin'}, {id:"2", name:'Dozent'}, {id:"3", name:'Kommilitone'}];
	}

	activate(params, routeConfig) {
		this.hint = "";
		this.password2 = "";
		this.changePW = false;
		this.routeConfig = routeConfig;
		this.account = Globals.CurrentAccount;

		return this.api.getUser(params.id).then(user => {
			if(!user) {
				return;
			}
			this.user = user;
			this.routeConfig.navModel.setTitle(user.name);
			this.originalUser = JSON.parse(JSON.stringify(user));
			this.ea.publish("UserViewed", this.user);
		});
	}

	get canSave() {
		return !this.api.isRequesting;
	}

	saveUser() {
		console.log(this.user);
		if(!this.user.username) {
			this.hint = "FEHLER: Der Benutzername darf nicht leer sein.";
			return false;
		}
		if(!this.user.password) {
			this.hint = "FEHLER: Das Passwort darf nicht leer sein.";
			return false;
		}
		if(this.changePW && this.user.password != this.password2) {
			this.hint = "FEHLER: Die angegebenen Passwörter müssen gleich sein.";
			return false;
		}
		this.changePW = false;
		this.api.saveUser(this.user).then(response => {
			if(response) {
				this.user.New = false;
				this.hint = "Benutzerdaten erfolgreich gespeichert";
				this.routeConfig.navModel.setTitle(this.user.name);
				this.originalUser = JSON.parse(JSON.stringify(this.user));
				this.ea.publish("UserUpdated", this.user);
			}
			else {
				let username = JSON.parse(JSON.stringify(this.user.username)); // copy of the string for appropriate view.
				this.hint = `FEHLER: Der Benutzername "${username}" ist bereits vergeben.`;
				this.user = JSON.parse(JSON.stringify(this.originalUser));
			}
		});
	}

	canDeactivate() {
		if(!this.originalUser || !this.user) {
			return true;
		}
		if (!areEqual(this.originalUser, this.user) || this.user.New){
			let result = confirm('You have unsaved changes. Are you sure you wish to leave?');
			if(!result) {
				this.ea.publish("UserViewed", this.user);
			}
			return result;
		}

		return true;
	}
	
	changePassword() {
		this.changePW = true;
		this.user.password = "";
		this.password2 = "";
	}
}