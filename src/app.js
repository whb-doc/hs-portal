import {EventAggregator} from 'aurelia-event-aggregator';
import Globals from './globals';

export class App {
	static inject() { return [EventAggregator]; }
	constructor(EventAggregator) {
		this.ea = EventAggregator;
		this.account = Globals.CurrentAccount;
		
		this.ea.subscribe("Login", response => {
			this.router.navigateToRoute('login');
		});
		
		this.ea.subscribe("LoggedInOut", response => {
			this.account = response;
			if(this.account) {			
				if(!this.currentRoute || this.currentRoute == 'login') {
					this.router.navigateToRoute('home');
				}
				else {
					this.router.navigateToRoute(this.currentRoute);
				}
			}
		});
		
		this.ea.subscribe("router:navigation:success", response => {
			let route = response.instruction.fragment;
			route = route.replace('/', '').split('/')[0];
			if(!route) {
				route = 'login';
			}
			this.currentRoute = route;
		});
	}
	
	configureRouter(config, router){
		config.title = 'OnlinePortal';
		config.map([
			{ route: ['', 'login'], moduleId: 'login', name: 'login'},
			{ route: 'home',  moduleId: 'home', name: 'home' },
			{ route: 'modules',  moduleId: 'modules', name: 'modules' },
			{ route: 'users',  moduleId: 'users', name: 'users' },
		]);

		this.router = router;
	}
  
	navigateTo(route) {
		this.router.navigateToRoute(route);
	}
}