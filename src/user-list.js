import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI} from './web-api';
import Globals from './globals';
import {Router} from 'aurelia-router';

export class UserList {
	static inject() { return [WebAPI, EventAggregator, Router] };

	constructor(api, EventAggregator, Router) {
		this.api = api;
		this.ea = EventAggregator;
		this.router = Router;
		this.users = [];
		
		this.ea.subscribe("UserViewed", user => this.select(user));
		this.ea.subscribe("UserUpdated", user => {
			let found = this.users.find(x => x.id === user.id);
			Object.assign(found, user);
			this.sortUsers();
		});
	}

	bind() {
		this.account = Globals.CurrentAccount;
		if(this.account.role == "1") {
			this.api.getUserList().then(response => {
				this.users = response;
				this.sortUsers();
				this.router.navigateToRoute('user', {id: this.users[0].id});
			});
		}
		else {
			this.users = [this.account];
			this.router.navigateToRoute('user', {id: this.users[0].id});
		}
	}
	
	sortUsers() {
		this.users.sort((x, y) => {
			return y.lastName < x.lastName;
		});
	}

	select(user) {
		this.selectedId = user.id;
		return true;
	}
	
	deleteUser(user) {
		if(confirm(`Möchten Sie wirklich den Benutzer ${user.firstName} ${user.lastName} löschen?`)) {
			this.api.removeUser(user.id).then(response => {
				if(response) {
					let index = this.users.indexOf(user);
					this.users.splice(index, 1);
				}
			});
		}
	}
	
	addUser() {
		this.api.addUser().then(newUser => {
			newUser.New = true;
			this.users.push(newUser);
			this.router.navigateToRoute('user', {id: newUser.id});
		});
	}
}