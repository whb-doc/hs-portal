import {EventAggregator} from 'aurelia-event-aggregator';
import {WebAPI, getId} from './web-api';
import Globals from './globals';

export class Home {
	static inject() { return [WebAPI, EventAggregator]; }
	constructor(api, EventAggregator) {
		this.api = api;
		this.ea = EventAggregator;
	}
	
	activate() {
		if(!Globals.CurrentAccount) {
			this.ea.publish("Login");
		}
	}
	
	bind() {
		this.api.getNews().then(response => {
			this.newsList = response;
			this.sortNews();
		});
	}
	
	dateToString(date) {
		let tempDate = new Date(date);
		let dd = tempDate.getDate();
		let mm = tempDate.getMonth() + 1; //January is 0!
		let yyyy = tempDate.getFullYear();
		if(dd < 10) {
			dd = '0' + dd
		}
		if(mm < 10) {
			mm = '0' + mm
		}
		return `${dd}.${mm}.${yyyy}`;
	}
	
	addNews() {
		let newNews = {id: getId(), heading: "Neuer Beitrag", description: "Beschreibung...", date: new Date(), archived : false};
		this.newsList.unshift(newNews);
	}
	
	saveNews(news) {
		news.date = new Date();
		this.api.saveNews(news).then(response => {
			this.sortNews();
		});
	}
	
	sortNews() {
		this.newsList.sort((x, y) => {
			return new Date(y.date) - new Date(x.date);
		});
	}
	
	deleteNews(news) {
		if(confirm(`Möchten Sie wirklich die Nachricht ${news.heading} löschen?`)) {
			this.api.removeNews(news.id).then(response => {
				if(response) {
					let index = this.newsList.indexOf(news);
					this.newsList.splice(index, 1);
				}
			});
		}
	}
}